<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dumbways</title>

    <style>
        *{
            margin:0;
            padding:0;
        }
        .container{
            background: url("../images/bg-black.jpg");
            height: 1000px;
        }

        nav{
            display: flex;
            justify-content: space-between;
            padding: 20px 35px;
            background: black;
            border-bottom: 2px solid white;
        }

        nav .logo {
            color: white;
        }

        nav .logo span{
            text-decoration: line-through;
        }

        nav ul {
            display: flex;
            list-style: none;
            
        }

        nav ul li button{
            padding: 5px 20px;
            text-decoration: none;
            color: white;
            margin: 0 10px;
            background: transparent;
            border-radius: 5px;
            transition: all 0.5s;
            border:1px solid white;
        }

        nav ul li button:hover{
            background: black;
            color: white;
            cursor: pointer;
            border-radius: 15px;
        }

        .content{
            /* border: 1px solid white; */
            box-sizing: border-box;
            padding: 20px 15px;
            color: black;
            /* border: 1px solid white; */
            background: transparent;
            width: 70%;
            display: flex;
            justify-content: space-evenly;
            box-shadow:
        }

        .content .card{
            border: 1px solid white;
            padding: 10px 10px; 
            margin: 0 5px;
            background: white;
            opacity: 0.4;
        }

        .content button{
            width: 50%;
            margin-top: 10px;
            padding: 2px 0;
            border: none;
            height: 30px;
            border-radius: 30px;
            background: black;
            color: white;
            transition: all 1s;
        }

        .content button:hover{
            background: white;
            color: black;
            border: 2px solid black;
            cursor: pointer;
        }

        .content .card .number{
            text-align: center;
            width: 20px;
            height: 20px;
            border: 1px solid black;
            border-radius: 50%;
            margin-bottom: 2px;
            background: black;
            color: white;
            /* margin-left: 60px; */
        }

        .content .card .card-head img{
            border: 1px solid white;
            width: 100%;
        }

        .content .card .card-isi{
            color:black;
            border: 1px solid black;
            width: 100%;
            padding: 5px 0;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <nav>
            <div class="logo">
                <h2>Pay <span>Music</span> Muratal</h2>
            </div>
            <ul>
                <li><button type="button">Add Singer</button></li>
                <li><button type="button">Add Genre</button></li>
                <li><button type="button">Add Music</button></li>
            </ul>
        </nav>

        <div class="content">

            <div class="card">
                <center><p class="number">1</p></center>
                <div class="card-head">
                    <img src="../images/about-model.png" alt="" >
                </div>

                <div class="card-isi">
                    <h4>925</h4>
                    <p>muzammil</p>
                </div>
                <center><button type="button">Detail</button></center>
            </div>

            <div class="card">
            <center><p class="number">2</p></center>
                <div class="card-head">
                    <img src="../images/about-model.png" alt="" >
                </div>

                <div class="card-isi">
                    <h4>925</h4>
                    <p>muzammil</p>
                </div>
                <center><button type="button">Detail</button></center>
            </div>

            <div class="card">
            <center><p class="number">3</p></center>
                <div class="card-head">
                    <img src="../images/about-model.png" alt="" >
                </div>

                <div class="card-isi">
                    <h4>925</h4>
                    <p>muzammil</p>
                </div>
                <center><button type="button">Detail</button></center>
            </div>

            <div class="card">
            <center><p class="number">4</p></center>
                <div class="card-head">
                    <img src="../images/about-model.png" alt="" >
                </div>

                <div class="card-isi">
                    <h4>925</h4>
                    <p>muzammil</p>
                </div>
                <center><button type="button">Detail</button></center>
            </div>

        </div>
    </div>
</body>
</html>